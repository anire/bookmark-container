/**
 * Opens a URL in the container specified by containerId.
 * @param {string} url - The url to open.
 * @param {string} containerId - The ID of the container to use.
 */
async function openURLInContainer(url, containerId) {
  if (!containerId) {
    await browser.tabs.create({
      active: true,
      url,
    });
    return;
  }
  await browser.tabs.create({
    active: true,
    url,
    cookieStoreId: containerId,
  });
}

/**
 * Factory for opening bookmarks in containers.
 * @param {string} containerId - The ID of the container to use.
 * @returns {function}
 */
function bookmarkContainerFactory(containerId) {
  return async (info) => {
    let bookmarks = await browser.bookmarks.get(info.bookmarkId);
    let bookmark = bookmarks[0];
    if (bookmark.type !== "bookmark") return;
    await openURLInContainer(bookmark.url, containerId);
  };
}

(async function() {
  let containers = await browser.contextualIdentities.query({});
  // Create holding menu item.
  let parentId = browser.menus.create({
    title: "Open in a New Container Tab",
    contexts: ["bookmark"],
  });
  for (let container of containers) {
    browser.menus.create({
      title: container.name,
      icons: {
        "16": container.iconUrl,
      },
      parentId,
      contexts: ["bookmark"],
      onclick: bookmarkContainerFactory(container.cookieStoreId),
    });
  }
})();
